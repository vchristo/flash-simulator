#include "simulatorMaster.h"
#include "simulaTest.h"
#include <gtest/gtest.h>
#include <inttypes.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <memory.h>
#include <unistd.h>
#include <pthread.h>
size_t sec =512;
size_t flz= 2097152;

using namespace std;
uint8_t *DataRead=NULL;


TEST(testPrepareMemory, PositiveNos) { 
    ASSERT_EQ(1, testPrepareMemory());
}

TEST(startThread, PositiveNos) { 
    ASSERT_EQ(1, startThread());

}

TEST(testPrepareSetFlashSize, PositiveNos) { 
    ASSERT_EQ(1, testPrepareSetFlashSize(flz,sec));
}

TEST(statusRegister_sector_erase_1, PositiveNos) { 
    ASSERT_EQ(2, statusRegister(6)); // 6 write enable 4 write disable 
    ASSERT_EQ(1, sectorErase(1));
}


TEST(testPrepareSetData, PositiveNos) { 
    ASSERT_EQ(1, testPrepareSetData(4,Data,DATA_SIZE));
 }

TEST(ReadData , PositiveNos) { 
    ASSERT_EQ(1, readData(DataRead,2048));
 }

int main(int argc, char **argv) {
    DataRead = (uint8_t *)malloc(SEC_SIZE);
    testing::InitGoogleTest(&argc, argv);
    int ret = RUN_ALL_TESTS();
    run=0;
    cleanUp(response);
    cleanUp(flash);
    cleanUp(working_buffer);
    cleanUp(DataRead);
    pthread_join(tid, NULL);
    return ret;
}

