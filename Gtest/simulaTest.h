#include <string.h>
bool testPrepareSetData(uint32_t address, uint8_t *data, uint32_t dataLength);
void testPrepareSetFlashSize(uint32_t totalSize, uint32_t blockSize); // this interface is in bytes
int testPrepareMemory(void); // initialized and clears the internal memory


uint8_t statusRegister(uint8_t reg);
bool compareSector(uint16_t sector);
bool sectorErase(uint32_t sec);
bool writeToFlashSector (uint32_t sec);
bool readData(uint8_t *DataRead,uint32_t address);
bool startThread();
SPIData data = { 0 };
//uint8_t *dataWrite=NULL;


bool testPrepareSetFlashSize(size_t totalSize, size_t blockSize ){ 

    printf("[ActaulSize] Flash size = %lu\n",FLASH_SIZE);
    printf("[ActaulSize] Sector size : %lu\n",SECTOR_SIZE);
    uint8_t * tmp=NULL;
    tmp=(uint8_t*)realloc(flash,totalSize);
    if (tmp == NULL)
    {
      printf("[   Memory   ] Error resizing memory !!!");
      return false;
    }
    else
    {
        flash = tmp;
    }
    
    FLASH_SIZE = totalSize;
    SECTOR_SIZE = blockSize;
    printf("[ Updated  ] flash size : %lu\n",FLASH_SIZE);
    printf("[ Updated  ] sector size : %lu\n",SECTOR_SIZE);
    printf("[ Fill 0   ] memory fill with zeros : %lu Bytes\n",FLASH_SIZE);
    memset(flash,0,FLASH_SIZE);
    return true;    

}

bool startThread(){
 pthread_create(&tid, NULL, simulator_thread, NULL);
return true;
}

int testPrepareMemory(void )
{
    cleanUp(flash);
    pthread_mutex_init(&mtx, NULL);
    flash =(uint8_t*) calloc(1, FLASH_SIZE*sizeof(uint8_t));
    if(flash == NULL) {
        printf("Error allocating memory\n");
        return -1;
    }
    working_buffer = (uint8_t*)calloc(1, WB_SIZE*sizeof(uint8_t));
    if(flash == NULL) {
        printf("Error allocating memory\n");
        return -1;
    }
    for(uint32_t i = 0; i < FLASH_SIZE; i++){  //init flash zeros
        flash[i]=0x0;
    }
    for(uint32_t i = 0; i < WB_SIZE*sizeof(uint8_t); i++){
        working_buffer[i]=0;
    }

    return 1;
}


uint8_t statusRegister(uint8_t reg){
   // Quick tests
    uint8_t resp;
  
        data.byte[0]=reg;
        writeOnSPI(data.byte, 1);
        data.byte[0] = READ_STATUS_REGISTER;
        printf("[  Status  ] request  Enable to write flash \n");
        writeOnSPI(data.byte, 1);
        readFromSPI(&resp, 1);
        printf("[   Read   ] readFromSPI Current status Register  %u\n", resp);
//        usleep(20000);
    return resp;
    
}

bool sectorErase(uint32_t sec){
    /*
    data.byte[0]=WRITE_ENABLE;
    writeOnSPI(data.byte, 4);
    */
    printf("[   WEL    ] Enable \n");
    uint32_t tmpSec=sec;
    uint32_t addr = sec*SECTOR_SIZE;
    addr = (uint32_t)addr << 8;
    data.integer = 0x00000020 | addr;
    printf("[Sec Erase ] Sector 0x%x   Address : 0x%zx  (Dec: %zu data.integer = 0x%x)\n",sec, tmpSec*SECTOR_SIZE,sec*SECTOR_SIZE,data.integer);
    writeOnSPI(data.byte, 4);
    pthread_mutex_lock(&mtx);
    for(uint32_t i = 0; i< SECTOR_SIZE; i++){
        if(flash[tmpSec*SECTOR_SIZE+i] != 0xff){
            printf("Flash erase error ... at %zu  %u\n",sec*SECTOR_SIZE+i , flash[sec*SECTOR_SIZE+i]);
            pthread_mutex_unlock(&mtx);
            writeOnSPI(data.byte, WRITE_DISABLE);
            printf("[   WEL    ] Disable \n");
            return false;
        }
    }

    
    pthread_mutex_unlock(&mtx);
    data.byte[0]=WRITE_DISABLE;
    printf("[   WEL    ] Disable \n");    
    writeOnSPI(data.byte, 4);
    return true;
}

bool compareSector(uint16_t sector){
    pthread_mutex_lock(&mtx);
    for(uint32_t i=0; i<DATA_SIZE; i++) {
    if(flash[sector*SECTOR_SIZE+i] != Data[i]){
                printf("[  ERROR   ]Return false for i = %d flash = 0x%02X Data= 0x%02x \n",i, flash[sector*SECTOR_SIZE+i],Data[i]);
                pthread_mutex_unlock(&mtx);
                return false;
        }
    }
    pthread_mutex_unlock(&mtx);
    return true;
}

bool readData(uint8_t *DataRead,uint32_t address){
    address = (uint32_t)address << 8; 
    data.integer = 0x00000003 | address;
    writeOnSPI(data.byte,4);
    readFromSPI(DataRead,(uint32_t)SECTOR_SIZE);
    //      [   WEL    ]
    printf("[  Dump    ] Flash read comparation with previously written data, add: %u size: %u sector %lu \n", address/0x100,DATA_SIZE, (address/0x100)/SECTOR_SIZE);
    for(uint32_t i=0; i<DATA_SIZE; i++) {
        if(DataRead[i] != Data[i])
        return 0;
        printf("%02X ", DataRead[i]);
        if((i+1)%8 == 0)
            printf(" ");
        if((i+1)%16 == 0)
            printf("\n");
    }
    printf("\n");
    return 1;
         
}

bool testPrepareSetData(uint32_t address, uint8_t *Data, uint32_t dataLength){  // Sector, Data, Data Length
    data.byte[0]=WRITE_ENABLE;
    writeOnSPI(data.byte, 4);
    uint32_t tmpSec=address;
    printf("[   WEL    ] Enabled \n");
        uint8_t *dataWrite=NULL;
        dataWrite=(uint8_t*)malloc(DATA_SIZE + 4);
        address *=SECTOR_SIZE;    
        address =(uint32_t)address << 8;
        data.integer = 0x00000002 | address; 
        printf("[  Write   ] writeOnSPI Address %u , Sector %u   Data size %u Plus data placed in config.h\n", address/0x100,tmpSec,DATA_SIZE);
        memcpy(dataWrite,data.byte,4);
        dataWrite +=4;
        memcpy(dataWrite,Data,DATA_SIZE);
        dataWrite -=4;
        writeOnSPI(dataWrite,DATA_SIZE+4);
        if(DATA_SIZE >  SECTOR_SIZE){
            printf("[----------]\n");
          data.byte[0]=WRITE_DISABLE;
          writeOnSPI(data.byte, 4);
          printf("[   WEL    ] Disable \n");              
          return false;
        }
    printf("[----------]\n");
    free(dataWrite);
    bool ret = compareSector(tmpSec); 
    data.byte[0]=WRITE_DISABLE;
    writeOnSPI(data.byte, 4);
    printf("[   WEL    ] Disable \n");
    return ret;
}
