#include"config.h"
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <memory.h>
#include <unistd.h>
#include <pthread.h>



typedef union
{
    struct
    {
        // Byte 1
        uint8_t wip : 1;
        uint8_t wel : 1;
        uint8_t bp0 : 1;
        uint8_t bp1 : 1;
        uint8_t bp2 : 1;
        uint8_t bp3 : 1;
        uint8_t bp4 : 1;
        uint8_t srp0 : 1;
        // Byte 2
        uint8_t srp1 : 1;
        uint8_t qe : 1;
        uint8_t sus2 : 1;
        uint8_t lb1 : 1;
        uint8_t lb2 : 1;
        uint8_t lb3 : 1;
        uint8_t cmp : 1;
        uint8_t sus1 : 1;
        // Byte 3
        uint8_t reserved : 5;
        uint8_t drv0 : 1;
        uint8_t drv1 : 1;
        uint8_t reserved2 : 1;
    };
    unsigned char byte[3];
} status_reg;

typedef union
{
    uint32_t integer;
    uint8_t byte[4];
} SPIData;

void writeOnSPI(uint8_t *data, uint32_t dataLength);
void readFromSPI(uint8_t *data, uint32_t dataLength);
void tick1ms();
uint8_t *flash = NULL;
uint8_t *working_buffer = NULL;
uint8_t *response = NULL;
bool run=1;
bool read_ready=0;
// unsigned long FLASH_SIZE = 2*1024*1024L;

pthread_mutex_t mtx;
pthread_t tid;

status_reg sr = { 0 };

void dumpFlashSector(uint16_t sector) {

    pthread_mutex_lock(&mtx);
    printf("Sector 0x%x size 0x%zx SECTOR_SIZE dump:\n", sector,SECTOR_SIZE);
    for(uint32_t i=0; i<SECTOR_SIZE; i++) {
        printf("%02X ", flash[sector*SECTOR_SIZE+i]);
        if((i+1)%8 == 0)
            printf(" ");
        if((i+1)%16 == 0)
            printf("\n");
    }
    printf("\n");
    pthread_mutex_unlock(&mtx);
}

void writeOnSPI(uint8_t *data, uint32_t dataLength) {
    pthread_mutex_lock(&mtx);
 //   memset(working_buffer, 0xff, WB_SIZE);
    memcpy(working_buffer, data, dataLength);
    pthread_mutex_unlock(&mtx);
    usleep(2000);
    return;
}

void readFromSPI(uint8_t *data, uint32_t dataLength) {
    uint32_t to = 0;
    while(to < READ_TIMEOUT) {
        pthread_mutex_lock(&mtx);
        if(read_ready) {
        if(sr.wip){
            uint8_t ret=0xff;
            memcpy(data,&ret,1);
            read_ready = 0;
            pthread_mutex_unlock(&mtx);
            break;
        }else{
        
            memcpy(data, response, dataLength);
            free(response);
            read_ready = 0;
            pthread_mutex_unlock(&mtx);
            break;
            }
        }
        pthread_mutex_unlock(&mtx);
        tick1ms();
        to++;
    }
    if(to >= READ_TIMEOUT) {
        printf("Read timeout\n");
    }

    usleep(2000);
    return;
}
void tick1ms() {
    usleep(1000);
}

static void *simulator_thread(void *arg)
{
    while(run) {
        pthread_mutex_lock(&mtx);
        switch(working_buffer[0]) {
             case 0x2:
             {   //printf("0x02\n");
                if(sr.wel) {
                    uint32_t sector;
                    uint32_t addr = *(uint32_t*)working_buffer >> 8;
                    if((addr & 0x000000ff)==0){
                        sector = addr/SECTOR_SIZE;
                    
                        if(sector <=TOTAL_SECTORS ){
                                        
                     //     [----------]
                            printf("[  addr    ] 0x%x sector=0x%x\n", addr, sector);
                            sr.wip = 1;
                            printf("[   WIP    ] Enabled \n");
                            sr.wel = 0;
                            working_buffer +=4;
                            if(DATA_SIZE <= SECTOR_SIZE ){
                                memcpy(&flash[sector*SECTOR_SIZE],working_buffer,DATA_SIZE);
                                printf("[ -->>>>   ] Write to flash ready \n");
                                working_buffer -=4;
                                sr.wip = 0;
                                
                            }else{
                                memcpy(&flash[sector*SECTOR_SIZE],working_buffer,SECTOR_SIZE);
                                printf("[ -->>>>   ] Write to flash incomlplete  \n");
                                working_buffer -=4;
                                
                                printf("[ Data Size] %u \n",DATA_SIZE);
                                printf("[SectorSize] %lu \n",SECTOR_SIZE);
                                printf("[  ERROR   ]*********************** ERROR *************************\n");
                                printf("[  ERROR   ]* Data size must be less or iqual to the sector size  *\n");
                                printf("[  ERROR   ]*******************************************************\n") ;
                                break;
                        
                            }
                                for(uint32_t i=0; i< SECTOR_ERASE_TIME; i++) {
                                tick1ms();
                            }
                        }else {
                                printf("[WriteError] sector  %u must be <= TOTAL_SECTORS  %lu \n",sector, TOTAL_SECTORS);
                                printf("[  ERROR   ]*********************** ERROR *************************\n");
                                printf("[  ERROR   ]*       the max sectort number is %06lu              *\n",TOTAL_SECTORS);
                                printf("[  ERROR   ]*******************************************************\n") ;
                                break;
                        }
                    }
                    else {
                                printf("[  Warning ] sector  overlapiing the next sector   \n");
                                printf("[  ERROR   ]*************************   Warnig   **************************\n");
                                printf("[  ERROR   ]* 8 least significant address bits (A7 - A0) are not all zero *\n");
                                printf("[  ERROR   ]***************************************************************\n") ;
                    }
                    printf("[   WIP    ] Disable \n");
                    sr.wel = 0;
                    printf("[   WEL    ] Disable \n");
                    break ;
                }
                }
                 
               case 3:
                {   
                    uint32_t addr = *(uint32_t*)working_buffer >> 8;
                    if(response != NULL)free(response);
                    response= (uint8_t *)calloc(1,SECTOR_SIZE);
                    memcpy(response, flash+addr,(uint32_t)SECTOR_SIZE);
                    read_ready = 1;                    
                    break;
                }    
                 
             case 0x4:
                {
                    sr.wel = 0;
                    break;
                
                }
 
                case 0x5:
                 {   
                            
                    response = (uint8_t *)calloc(1, sizeof(uint8_t));
                    memcpy(response, &sr.byte[0], sizeof(uint8_t));
                    read_ready = 1;
                    break;
                
                }
                 
            case 0x06:
            {    // Enable WIP
                 sr.wel = 1;
                break;
            } 
                
            case 0x20:
            {
                // Sector Erase
                printf("0x20\n");
                if(sr.wel) {
                    uint32_t sector;
                    uint32_t addr = *(uint32_t*)working_buffer >> 8;

                    if(addr == 0) {
                        sector = 0;
                    }
                    else {
                        sector = addr/SECTOR_SIZE;
                    }
                    printf("addr=%u sector %u\n", addr, sector);
                    sr.wip = 1;
                    sr.wel = 0;
                    for(uint32_t i=0; i<SECTOR_ERASE_TIME; i++) {
                        tick1ms();
                    }
                    memset(&flash[sector*SECTOR_SIZE], 0xff, SECTOR_SIZE);
                    sr.wip = 0;
                }
                    break;
                }
                 
    }
                
        
        memset(working_buffer, 0, WB_SIZE);
        pthread_mutex_unlock(&mtx);
        usleep(1000);
    }
    return NULL;
}

int main(int argc, char *argv[])
{
    pthread_mutex_init(&mtx, NULL);

    flash = calloc(1, FLASH_SIZE*sizeof(uint8_t));
    if(flash == NULL) {
        printf("Error allocating memory\n");
        return -1;
    }

    working_buffer = calloc(1, WB_SIZE*sizeof(uint8_t));
    if(flash == NULL) {
        printf("Error allocating memory\n");
        return -1;
    }

    pthread_create(&tid, NULL, simulator_thread, NULL);

    SPIData data = { 0 };

    // Quick tests
    uint8_t resp;
    

    // Status register 1 read
    data.byte[0] = 0x05;
    writeOnSPI(data.byte, 4);
    readFromSPI(&resp, 1);
    printf("resp %u\n", resp);
    

    // Write enable
    data.byte[0] = 0x06;
    writeOnSPI(data.byte, 4);

    // Status register 1 read
    data.byte[0] = 0x05;
    writeOnSPI(data.byte, 4);
    readFromSPI(&resp, 1);
    printf("resp %u\n", resp);

    /*
    // Write disable
    data.byte[0] = 0x04;
    writeOnSPI(data.byte, 4);
    */

    // Status register 1 read
    data.byte[0] = 0x05;
    writeOnSPI(data.byte, 4);
    readFromSPI(&resp, 1);
    printf("%u\n", resp);

    // Sector erase
    data.integer = 0x00001020;
    writeOnSPI(data.byte, 4);

    // Sector  write

    uint8_t toSend[]="1234567890123456";
    uint32_t size=sizeof(toSend)-1;
    sr.wel = 1;
    uint8_t *dataWrite=(uint8_t*)malloc(size + 4);
    data.integer = 0x00000002;
    memcpy(dataWrite,data.byte,4);
    dataWrite +=4;
    memcpy(dataWrite,toSend,size);
    dataWrite -=4;
    writeOnSPI(dataWrite,size+4);
    free(dataWrite);

    run=0;

    dumpFlashSector(0);
    dumpFlashSector(1);
    dumpFlashSector(2);

    pthread_join(tid, NULL);
    free(working_buffer);
    free(flash);
}

